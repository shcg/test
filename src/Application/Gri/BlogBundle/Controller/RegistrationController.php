<?php
namespace Application\Gri\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Application\Gri\BlogBundle\Form\UserType;
use Application\Gri\BlogBundle\Entity\User;
use Application\Gri\BlogBundle\Entity\Role;
use Symfony\Component\HttpFoundation\Request;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;

class RegistrationController extends Controller
{
    /**
     * @Route("/register", name="user_registration")
     */
    public function registerAction(Request $request)
    {
        // 1) build the form
        $user = new User();
        $form = $this->createForm(new UserType(), $user);

        // 2) handle the submit (will only happen on POST)
        $form->handleRequest($request);
        if ($form->isValid() && $form->isSubmitted()) {

            $role = new Role();
            $role->setName('ROLE_USER');

            $user->setSalt(md5(time()));
            $user->setUsername($user->getEmail());
            $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
            $password = $encoder->encodePassword($user->getPassword(), $user->getSalt());
            $user->setPassword($password);
            $user->getUserRoles()->add($role);

            $user->setStatus(1);

            // 4) save the User!
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->persist($role);
            $em->flush();

            // ... do any other work - like send them an email, etc
            // maybe set a "flash" success message for the user

            return $this->redirectToRoute('user_login');
        }

        return $this->render(
            'ApplicationGriBlogBundle:Registration:registration.html.twig',
            array('form' => $form->createView())
        );
    }
}