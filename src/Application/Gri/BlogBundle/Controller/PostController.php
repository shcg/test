<?php

namespace Application\Gri\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
 
class PostController extends Controller
{
    public function indexAction(Request $request)
    {

	    $em    = $this->get('doctrine.orm.entity_manager');
	    $dql   = "SELECT a FROM ApplicationGriBlogBundle:Post a";
	    $query = $em->createQuery($dql);

	    $paginator  = $this->get('knp_paginator');
	    $pagination = $paginator->paginate(
	         $query,
	         $request->query->get('page', 1),
	         3
	    );

	    return $this->render('ApplicationGriBlogBundle:Post:index.html.twig', array('pagination' => $pagination));
    }

    public function postAction($id)
    {
	    $em = $this->getDoctrine()->getManager();

	    $post = $em->getRepository('ApplicationGriBlogBundle:Post')->find($id);

	    if (!$post) {
	        throw $this->createNotFoundException('Unable to find Post entity.');
	    }
	    return $this->render('ApplicationGriBlogBundle:Post:post.html.twig', array('post' => $post));
    }
}