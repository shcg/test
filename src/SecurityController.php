<?php

namespace Application\Gri\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SecurityController extends Controller
{
    public function loginAction()
    {
        return $this->render('ApplicationGriBlogBundle:Security:login.html.twig');
    }
}
