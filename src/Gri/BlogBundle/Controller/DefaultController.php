<?php

namespace Gri\BlogBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('GriBlogBundle:Default:index.html.twig', array('name' => $name));
    }
}
