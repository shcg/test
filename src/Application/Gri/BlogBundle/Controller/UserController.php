<?php
namespace Application\Gri\BlogBundle\Controller;
 
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Symfony\Component\HttpFoundation\Request;
use Application\Gri\BlogBundle\Entity\Post;
use Application\Gri\BlogBundle\Entity\User;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints as Recaptcha;
use EWZ\Bundle\RecaptchaBundle\Validator\Constraints\IsTrue as RecaptchaTrue;
use Application\Gri\BlogBundle\Form\PostType;
 
class UserController extends Controller
{
	public function indexAction(Request $request)
	{
	    $post = new Post();
	    $form = $this->createForm(new PostType(), $post);

	    $form->handleRequest($request);

	    if ($form->isValid()) {

	    	$user = $this->get('security.context')->getToken()->getUser();

            $user_entity = $this->getDoctrine()
		        ->getRepository('ApplicationGriBlogBundle:User')
		        ->find($user->getId());
	    	$post->setUser($user_entity);

	        $em = $this->getDoctrine()->getManager();

	        $em->persist($post);
	        $em->flush();

	        return $this->redirectToRoute('user_home');
	    }

        return $this->render(
            'ApplicationGriBlogBundle:User:index.html.twig',
            array('form' => $form->createView())
        );
	}
}