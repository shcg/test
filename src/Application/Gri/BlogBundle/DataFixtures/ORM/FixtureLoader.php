<?php
namespace Application\Gri\BlogBundle\DataFixtures\ORM;
 
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Application\Gri\BlogBundle\Entity\User;
use Application\Gri\BlogBundle\Entity\Role;
use Symfony\Component\Security\Core\Encoder\MessageDigestPasswordEncoder;
 
class FixtureLoader implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $role = new Role();
        $role->setName('ROLE_USER');
 
        $manager->persist($role);
 
        // создание пользователя
        $user = new User();
        $user->setEmail('test@test.ru');
        $user->setUsername('test2');
        $user->setStatus(1);
        $user->setSalt(md5(time()));
 
        // шифрует и устанавливает пароль для пользователя,
        // эти настройки совпадают с конфигурационными файлами
        $encoder = new MessageDigestPasswordEncoder('sha512', true, 10);
        $password = $encoder->encodePassword('123', $user->getSalt());
        $user->setPassword($password);
        $user->getUserRoles()->add($role);
 
        $manager->persist($user);
        $manager->flush();

    }

 
}